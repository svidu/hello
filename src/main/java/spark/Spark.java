package spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

/**
 * Created by root on 29.03.17.
 */
public class Spark {


    public static void main(String[] args) {
        SparkConf config = new SparkConf();
        config.setAppName("myApp");
        config.setMaster("local[1]");
        JavaSparkContext sc = new JavaSparkContext(config);

        JavaRDD<String> lines = sc.textFile("/home/sa/Spark");
        lines.map(String::toLowerCase)
                .flatMap(WordsUtil::getWords)
                .mapToPair(word->new Tuple2<>(word,1))
                .reduceByKey((a,b)->a+b)
                .mapToPair(Tuple2::swap)
                .sortByKey()
                .foreach(pair-> System.out.println(pair._1()+" "+pair._2()));
    }
}
