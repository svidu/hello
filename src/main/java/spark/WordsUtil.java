package spark;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by root on 29.03.17.
 */
public class WordsUtil {
    public static Iterator<String> getWords(String line){
        List<String> words = new ArrayList<>();
        Arrays.stream(line.split("[\\p{Punct} ]")).forEach(words::add);
        return words.iterator();
    }
}
